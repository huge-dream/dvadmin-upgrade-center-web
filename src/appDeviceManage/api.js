import { request } from '@/api/service'

export const urlPrefix = '/api/dvadmin_upgrade_center/app_device_manage/'

export function GetList (query) {
  return request({
    url: urlPrefix,
    method: 'get',
    params: query
  })
}
export function createObj (obj) {
  return request({
    url: urlPrefix,
    method: 'post',
    data: obj
  })
}

export function UpdateObj (obj) {
  return request({
    url: urlPrefix + obj.id + '/',
    method: 'put',
    data: obj
  })
}
export function DelObj (id) {
  return request({
    url: urlPrefix + id + '/',
    method: 'delete',
    data: { id }
  })
}

export function InternalTestDevice (query) {
  return request({
    url: '/api/dvadmin_upgrade_center/internal_test_device/',
    method: 'get',
    params: query
  })
}
