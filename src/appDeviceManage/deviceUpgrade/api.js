import { request } from '@/api/service'

export const urlPrefix = '/api/dvadmin_upgrade_center/device_upgrade/'

export function GetList (query) {
  return request({
    url: urlPrefix,
    method: 'get',
    params: query
  })
}
export function createObj (obj) {
  return request({
    url: urlPrefix,
    method: 'post',
    data: obj
  })
}

export function UpdateObj (obj) {
  return request({
    url: urlPrefix + obj.id + '/',
    method: 'put',
    data: obj
  })
}
export function DelObj (id) {
  return request({
    url: urlPrefix + id + '/',
    method: 'delete',
    data: { id }
  })
}

// 版本回滚
export function CancelUpdate (data) {
  return request({
    url: '/api/dvadmin_upgrade_center/cancel_update/',
    method: 'post',
    data
  })
}
