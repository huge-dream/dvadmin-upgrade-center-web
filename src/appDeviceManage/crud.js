import {urlPrefix as applicationPrefix} from "../application/api";
import {request} from "@/api/service";
export const crudOptions = (vm) => {
  return {
    pageOptions: {
      compact: true
    },
    options: {
      height: '100%'

    },
    viewOptions: {
      componentType: 'form'
    },
    formOptions: {
      defaultSpan: 24, // 默认的表单 span
      labelWidth: '130px',
      width: '35%'
    },
    rowHandle: {
      fixed: 'right',
      width: 200,
      view: {
        thin: true,
        text: '',
        show: false
      },
      edit: {
        thin: true,
        text: '',
        show: false
      },
      remove: {
        thin: true,
        text: '',
        show: false
      },
      custom: [// 自定义按钮
        {
          thin: true,
          text: '设备更新进度',
          type: 'success',
          size: 'small',
          // 点击事件,需要在<d2-crud-x @custom-emit="yourHandle"/>
          emit: 'deviceUpgrade'
        },{
          thin: true,
          text: '升级日志',
          type: 'info',
          size: 'small',
          // 点击事件,需要在<d2-crud-x @custom-emit="yourHandle"/>
          emit: 'upgradeLogging'
        }
      ]
    },
    indexRow: { // 或者直接传true,不显示title，不居中
      title: '序号',
      align: 'center',
      width: 60
    },

    columns: [
      {
        title: '编码',
        key: 'id',
        show: false,
        width: 80,
        form: {
          disabled: true
        }
      },
      {
        title: '设备标识号',
        key: 'device_id',
        width: 220,
        search: {
          disabled: false
        },
        type: 'input',
        form: {
          component: {
            placeholder: '请输入设备标识号',
            props: {
              clearable: true
            },
            disabled (context) {
              // 禁止编辑模式下使用
              const { mode } = context
              return mode === 'edit'
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请输入设备标识号' }
          ],
          helper: {
            render () {
              return (< el-alert title="设备标识的唯一性" type="warning" />
              )
            }
          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },{
        title: '所属应用',
        key: 'application',
        width: 120,
        search: {
          disabled: false,
        },
        type: 'select',
        dict: {
          cache: false,
          url: applicationPrefix,
          value: 'id', // 数据字典中value字段的属性名
          label: 'name', // 数据字典中label字段的属性名
          getData: (url) => { // 配置此参数会覆盖全局的getRemoteDictFunc
            return request({ url: url, params: { limit: 999, status: 1 } }).then(ret => {
              return ret.data.data
            })
          }
        },
        form: {
          component: {
            placeholder: '请选择所属应用',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请选择所属应用' }
          ],
          itemProps: {
            class: { yxtInput: true }
          },
          valueChange (key, value, form, { getComponent }) {
            form.app_version = undefined //
            console.log(form,)
            if (value) {
              getComponent('app_version').reloadDict()
            }
          },
          valueChangeImmediate: true,
        }
      },{
        title: '当前版本号',
        key: 'version_number',
        search: {
          disabled: false
        },
        width: 120,
        form: {
          component: {
            placeholder: '请选择当前版本号',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请选择当前版本号' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },{
        title: '升级前版本号',
        key: 'before_version_number',
        search: {
          disabled: false
        },
        width: 120,
        form: {
          component: {
            placeholder: '请选择升级前版本号',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请选择升级前版本号' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },  {
        title: '常在设备IP',
        key: 'online_ip',
        search: {
          disabled: true
        },
        width: 120,
        type: 'input',
        rowSlot: true,
        form: {
          disabled: false
        }
      },
      {
        title: '版本发布平台',
        key: 'platform_type',
        search: {
          disabled: false
        },
        width: 100,
        type: 'select',
        dict: {
          data: vm.dictionary('upgrade_center_platform')
        },
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '版本发布平台必填项' }
          ],
          component: {
            placeholder: '请选择版本发布平台'

          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      }
    ].concat(vm.commonEndColumns({}))
  }
}
