import { request } from '@/api/service'

export const urlPrefix = '/api/dvadmin_upgrade_center/upgrade_logging/'

export function GetList (query) {
  return request({
    url: urlPrefix,
    method: 'get',
    params: query
  })
}
