export const crudOptions = (vm) => {
  return {
    pageOptions: {
      compact: true
    },
    options: {
      height: '100%'

    },
    viewOptions: {
      componentType: 'form'
    },
    formOptions: {
      defaultSpan: 24, // 默认的表单 span
      labelWidth: '130px',
      width: '35%'
    },
    rowHandle: {
      width: 220,
      view: {
        thin: true,
        text: '',
        disabled () {
          return !vm.hasPermissions('Retrieve')
        }
      },
      edit: {
        thin: true,
        text: '',
        disabled () {
          return !vm.hasPermissions('Update')
        }
      },
      remove: {
        thin: true,
        text: '',
        disabled () {
          return !vm.hasPermissions('Delete')
        }
      },
      custom: [// 自定义按钮
        {
          thin: true,
          text: '版本管理',
          type: 'warning',
          size: 'small',
          // 点击事件,需要在<d2-crud-x @custom-emit="yourHandle"/>
          emit: 'on-version'
        }
      ]
    },
    indexRow: { // 或者直接传true,不显示title，不居中
      title: '序号',
      align: 'center',
      width: 100
    },

    columns: [
      {
        title: '编码',
        key: 'id',
        show: false,
        form: {
          disabled: true
        }
      },
      {
        title: '应用名称',
        key: 'name',
        search: {
          disabled: false,
        },
        type: 'input',
        form: {
          component: {
            placeholder: '请输入应用名称',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请输入应用名称' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
      {
        title: '应用标识',
        key: 'app_id',
        search: {
          disabled: false
        },
        type: 'input',
        form: {
          component: {
            placeholder: '请输入应用标识',
            props: {
              clearable: true
            },
            disabled (context) {
              // 禁止编辑模式下使用
              const { mode } = context
              return mode === 'edit'
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请输入应用标识' }
          ],
          helper: {
            render () {
              return (< el-alert title="用户标识应用的唯一性" type="warning" />
              )
            }
          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      }, {
        title: '最新版本下载地址',
        show: false,
        key: 'download_url',
        search: {
          disabled: true
        },
        type: 'input',
        rowSlot: true,
        form: {
          disabled:true,
          component: {
            placeholder: '请输入软件下载地址',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: false, type:'url', message: '请输入正确的下载地址' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
    ].concat(vm.commonEndColumns({    description: {
        showForm: true,
        showTable: true
      },}))
  }
}
