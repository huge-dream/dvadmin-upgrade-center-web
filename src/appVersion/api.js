import { request } from '@/api/service'

export const urlPrefix = '/api/dvadmin_upgrade_center/app_version/'

export function GetList (query) {
  return request({
    url: urlPrefix,
    method: 'get',
    params: query
  })
}

export function createObj (obj, id) {
  const data = { ...obj, application: id }
  return request({
    url: urlPrefix,
    method: 'post',
    data: data
  })
}

export function UpdateObj (obj) {
  return request({
    url: urlPrefix + obj.id + '/',
    method: 'put',
    data: obj
  })
}

export function DelObj (id) {
  return request({
    url: urlPrefix + id + '/',
    method: 'delete',
    data: { id }
  })
}
// 发布上线
export function ReleaseOnline (id) {
  return request({
    url: '/api/dvadmin_upgrade_center/release_online/',
    method: 'post',
    data: { id }
  })
}

// 版本回滚
export function VersionRollback (data) {
  return request({
    url: '/api/dvadmin_upgrade_center/version_rollback/',
    method: 'post',
    data
  })
}

// 版本下线
export function VersionOffline (data) {
  return request({
    url: '/api/dvadmin_upgrade_center/app_version/version_offline/',
    method: 'post',
    data
  })
}
