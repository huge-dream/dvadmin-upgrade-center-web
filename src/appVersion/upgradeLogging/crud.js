export const crudOptions = (vm) => {
  return {
    pageOptions: {
      compact: true
    },
    options: {
      tableType: 'vxe-table',
      rowKey: true, // 必须设置，true or false
      rowId: 'id',
      height: '100%', // 表格高度100%, 使用toolbar必须设置
      highlightCurrentRow: false

    },
    rowHandle: {
      fixed: 'right',
      view: {
        thin: true,
        text: '',
        disabled () {
          return !vm.hasPermissions('Retrieve')
        }
      },
      width: 70,
      edit: {
        thin: true,
        text: '',
        show: false,
        disabled () {
          return !vm.hasPermissions('Update')
        }
      },
      remove: {
        thin: true,
        text: '删除',
        show: false,
        disabled () {
          return !vm.hasPermissions('Delete')
        }
      }
    },
    viewOptions: {
      componentType: 'form'
    },
    formOptions: {
      appendToBody: true, // 子表格必须 否则弹出对话框无法显示最顶层
      disabled: true,
      defaultSpan: 12 // 默认的表单 span
    },
    indexRow: { // 或者直接传true,不显示title，不居中
      title: '序号',
      align: 'center',
      width: 70
    },
    columns: [
      {
        title: 'ID',
        key: 'id',
        width: 90,
        disabled: true,
        form: {
          disabled: true
        }
      },{
        title: '所属设备',
        key: 'device_id',
        search: {
          disabled: false
        },
        width: 220,
        type: 'input',
        form: {
          disabled: true,
          component: {
            placeholder: '请输入所属设备'
          }
        }
      },{
        title: '升级前版本',
        key: 'before_version_number',
        search: {
          disabled: true
        },
        width: 120,
        form: {
          component: {
            placeholder: '请选择升级版本',
            props: {
              clearable: true
            }
          },
          rules: [ // 表单校验规则
            { required: true, message: '请选择升级版本' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
      {
        title: '日志类型',
        key: 'logging_type',
        search: {
          disabled: false
        },
        width: 100,
        type: 'select',
        dict: {
          data: vm.dictionary('upgrade_center_logging_type')
        },
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '日志类型必填项' }
          ],
          component: {
            placeholder: '请选择日志类型'

          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
      {
        title: '登录ip',
        key: 'ip',
        search: {
          disabled: false
        },
        width: 130,
        type: 'input',
        form: {
          disabled: true,
          component: {
            placeholder: '请输入登录ip'
          }
        }
      }, {
        title: '运营商',
        key: 'isp',
        search: {
          disabled: true
        },
        disabled: true,
        width: 180,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入运营商'
          }
        }
      }, {
        title: '大州',
        key: 'continent',
        width: 80,
        type: 'input',
        form: {
          disabled: true,
          component: {
            placeholder: '请输入大州'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '国家',
        key: 'country',
        width: 80,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入国家'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '省份',
        key: 'province',
        width: 80,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入省份'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '城市',
        key: 'city',
        width: 80,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入城市'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '县区',
        key: 'district',
        width: 80,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入县区'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '区域代码',
        key: 'area_code',
        width: 100,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入区域代码'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '英文全称',
        key: 'country_english',
        width: 120,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入英文全称'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '简称',
        key: 'country_code',
        width: 100,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入简称'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '经度',
        key: 'longitude',
        width: 80,
        type: 'input',
        disabled: true,
        form: {
          component: {
            placeholder: '请输入经度'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '纬度',
        key: 'latitude',
        width: 80,
        type: 'input',
        disabled: true,
        form: {
          component: {
            placeholder: '请输入纬度'
          }
        },
        component: { props: { color: 'auto' } } // 自动染色
      }, {
        title: '操作系统',
        key: 'os',
        width: 180,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入操作系统'
          }
        }
      }, {
        title: '浏览器名',
        key: 'browser',
        width: 180,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入浏览器名'
          }
        }
      }, {
        title: 'agent信息',
        key: 'agent',
        disabled: true,
        width: 180,
        type: 'input',
        form: {
          component: {
            placeholder: '请输入agent信息'
          }
        }
      }, {
        fixed: 'right',
        title: '升级请求时间',
        key: 'create_datetime',
        width: 160,
        type: 'datetime'
      }
    ]
  }
}
