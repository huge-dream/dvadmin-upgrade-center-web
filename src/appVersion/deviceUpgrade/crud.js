export const crudOptions = (vm) => {
  return {
    pageOptions: {
      compact: true
    },
    options: {
      height: '100%'

    },
    viewOptions: {
      componentType: 'form'
    },
    formOptions: {
      appendToBody: true, // 子表格必须 否则弹出对话框无法显示最顶层
      defaultSpan: 24, // 默认的表单 span
      labelWidth: '130px',
      width: '35%'
    },
    rowHandle: {
      fixed: 'right',
      width: 90,
      view: {
        thin: false,
        text: '',
        show:false,
        disabled () {
          return !vm.hasPermissions('Retrieve')
        }
      },
      edit: {
        thin: false,
        text: '',
        show:false,
        disabled () {
          return !vm.hasPermissions('Update')
        }
      },
      remove: {
        thin: false,
        text: '',
        show:false,
        disabled () {
          return !vm.hasPermissions('Delete')
        }
      },
      custom: [// 自定义按钮
        {
          thin: true,
          text: '取消更新',
          type: 'warning',
          size: 'small',
          // 点击事件,需要在<d2-crud-x @custom-emit="yourHandle"/>
          disabled(index,row) {
            return [0,1].indexOf(row.status) === -1
          },
          emit: 'cancelUpdate'
        }
      ]
    },
    indexRow: { // 或者直接传true,不显示title，不居中
      title: '序号',
      align: 'center',
      width: 50
    },

    columns: [
      {
        title: '编码',
        key: 'id',
        show: false,
        form: {
          disabled: true
        }
      },{
        title: '所属设备', // table
        key: 'device_id',
        search: {
          disabled: false
        },
        type: 'input',
        width: 210,
        form: {
          disabled: false,
          component: {
            placeholder: '请输入所属设备'
          }
        }
      },
      {
        title: '升级类型',
        key: 'upgrade_type',
        search: {
          disabled: false
        },
        width: 100,
        type: 'select',
        dict: {
          data: vm.dictionary('upgrade_center_upgrade_type')
        },
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '升级类型必填项' }
          ],
          component: {
            placeholder: '请选择升级类型'

          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
      {
        title: '是否强制',
        key: 'is_coerce_upgrade',
        search: {
          disabled: true
        },
        width: 80,
        type: 'select',
        dict: {
          data: vm.dictionary('button_whether_bool')
        },
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '是否强制必填项' }
          ],
          component: {
            placeholder: '请选择是否强制'

          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
      {
        title: '升级状态',
        key: 'status',
        search: {
          disabled: false
        },
        width: 100,
        type: 'select',
        dict: {
          data: vm.dictionary('upgrade_center_upgrade_status')
        },
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '升级状态必填项' }
          ],
          component: {
            placeholder: '请选择升级状态'

          },
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
    ].concat(vm.commonEndColumns({
      update_datetime: {
        showForm: false,
        showTable: false
      }
    }))
  }
}
