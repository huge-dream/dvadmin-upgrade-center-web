import { request } from '@/api/service'

// 发起内测
export function InternalTest (data) {
  return request({
    url: '/api/dvadmin_upgrade_center/internal_test/',
    method: 'post',
    data
  })
}
