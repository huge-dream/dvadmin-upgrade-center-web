export const crudOptions = (vm) => {
  return {
    pageOptions: {
      compact: true
    },
    options: {
      tableType: 'vxe-table',
      rowKey: false,
      width: '100%',
      height: '100%' // 表格高度100%, 使用toolbar必须设置
    },
    rowHandle: {
      dropdown: {
        atLeast: 4 // 至少2个以上才收入下拉框中
      },
      fixed: 'right',
      width: 230,
      view: {
        thin: true,
        text: ''
      },
      edit: {
        thin: true,
        text: '',
        disabled(index,row){return [0,3,4].indexOf(row.status) === -1}
      },
      remove: {
        thin: true,
        text: '',
        disabled(index,row){return [0,3,4].indexOf(row.status) === -1}
      },
      custom: [
        {
          text: '发布上线',
          type: 'warning',
          size: 'small',
          emit: 'releaseOnline',
          // 回滚后的版本，不允许再上线
          disabled(index,row){return [0,2,3,4].indexOf(row.status) === -1}
        },
        {
          text: '版本回滚',
          type: 'warning',
          size: 'small',
          emit: 'versionRollback',
          disabled(index,row){return [1].indexOf(row.status) === -1}
        },
        {
          text: '灰度内测',
          type: 'warning',
          size: 'small',
          emit: 'grayscaleBeta',
          // 待发布和下线状态可灰度内测，或当前为灰度内测
          disabled(index,row){return [0,2,3,4].indexOf(row.status) === -1}
        },
        {
          text: '设备更新进度',
          type: 'warning',
          size: 'small',
          emit: 'deviceUpgrade',
        },
        {
          text: '升级日志',
          type: 'warning',
          size: 'small',
          emit: 'upgradeLogging',
        },
        {
          text: '版本下线',
          type: 'warning',
          size: 'small',
          emit: 'versionOffline',
          disabled(index,row){return [1].indexOf(row.status) === -1}
        }
      ]
    },
    indexRow: { // 或者直接传true,不显示title，不居中
      title: '序号',
      align: 'center',
      width: 70
    },
    viewOptions: {
      componentType: 'form'
    },
    formOptions: {
      defaultSpan: 24, // 默认的表单 span
      labelWidth: '130px',
      width: '35%'
    },
    columns: [
    {
      title: 'ID',
      key: 'id',
      show: false,
      width: 90,
      form: {
        disabled: true
      }
    },
      {
        title: '更新标题',
        key: 'title',
        search: {
          disabled: false
        },
        type: 'input',
        width: 180,
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '更新标题必填项' }
          ],
          component: {
            placeholder: '请输入更新标题'
          },

          itemProps: {
            class: { yxtInput: true }
          }
        }
      },{
        title: '更新版本号',
        key: 'version_number',
        search: {
          disabled: false
        },
        width: 100,
        type: 'input',
        form: {
          rules: [ // 表单校验规则
            { required: true, message: '更新版本号必填项' },
          ],
          component: {
            placeholder: '请输入更新版本号'
          },
          itemProps: {
            class: { yxtInput: true }
          },
          helper: '建议版本号请以vx.x.x的形式，例如v0.0.1（与客户端的版本格式保持一致）'
        }
      },
      {
        title: '更新内容',
        key: 'content',
        search: {
          disabled: true
        },
        show: false,
        width: 200,
        type: 'textarea',
        form: {
          rules: [ // 表单校验规则
            { max: 1000, message: '最大输入1000，请重新输入', trigger: 'blur' },
            { required: true, message: '必填项' }
          ],
          component: {
            span: 24,
            placeholder: '请输入更新内容',
            showWordLimit: true,
            props: {
              type: 'textarea',
              autosize: {
                minRows: 2, maxRows: 8
              }
            }
          },

          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
    {
      title: '升级类型',
      key: 'upgrade_type',
      search: {
        disabled: true
      },
      width: 100,
      type: 'radio',
      dict: {
        data: [
          { label: '整包升级', value: 0 },
          { label: '部分升级', value: 1 }
        ]
      },
      form: {
        value: 1,
        rules: [ // 表单校验规则
          { required: true, message: '必填项' }
        ],
        itemProps: {
          class: { yxtInput: true }
        }
      }
    },{
      title: '强制升级',
      key: 'is_coerce_upgrade',
      search: {
        disabled: true
      },
        width: 80,
      type: 'radio',
      dict: {
        data:vm.dictionary('button_whether_bool')
      },
      form: {
        value: false,
        rules: [ // 表单校验规则
          { required: true, message: '必填项' }
        ],
        itemProps: {
          class: { yxtInput: true }
        }
      }
    },
    {
      title: '版本发布平台',
      key: 'platform',
      search: {
        disabled: false
      },
      width: 100,
      type: 'select',
      dict: {
        data: vm.dictionary('upgrade_center_platform')
      },
      form: {
        rules: [ // 表单校验规则
          { required: true, message: '版本发布平台必填项' }
        ],
        component: {
          placeholder: '请选择版本发布平台'

        },
        itemProps: {
          class: { yxtInput: true }
        }
      }
    },
    {
      title: '版本文件',
      key: 'download_url',
      search: {
        disabled: true
      },
      width: 100,
      rowSlot: true,
      form: {
        component: {
          name: 'd2p-file-uploader',
          placeholder: '请上传版本文件',
          props: {
            clearable: true,
            returnType: 'object',
            elProps: { // 与el-uploader 配置一致
              multiple: false,
              limit: 1, // 限制5个文件
            },
          },
          sizeLimit: 200000 * 1024 // 不能超过限制200M
        },
        rules: [ // 表单校验规则
          { required: true, message: '版本文件不能为空' }
        ],
        itemProps: {
          class: { yxtInput: true }
        },
        valueChange(key ,value ,form){
          if (value != null && value instanceof Object) {
            form.file_size = value.size
            form.file_md5 = value.md5
            form.download_url = form.download_url.url
          }
        },
      }
    },{
      title: '文件大小',
      key: 'file_size',
      search: {
        disabled: true
      },
        width: 100,
      form: {
        component: {
          placeholder: '请上传文件，大小自动获取',
          props: {
            clearable: true,
            disabled: true,
            elProps: { // 与el-uploader 配置一致
            },
          },
        },
        itemProps: {
          class: { yxtInput: true }
        }
      }
    },{
      title: '文件MD5',
      key: 'file_md5',
      search: {
        disabled: true
      },
        width: 160,
        show: false,
      form: {
        component: {
          placeholder: '请上传文件，MD5自动获取',
          props: {
            clearable: true,
            disabled: true,
            elProps: { // 与el-uploader 配置一致
            },
          },
        },
        itemProps: {
          class: { yxtInput: true }
        }
      }
    },
    {
      title: '版本状态',
      key: 'status',
      search: {
        disabled: true
      },
      width: 100,
      type: 'select',
      dict: {
        data: vm.dictionary('upgrade_center_version_status')
      },
      form: {
        value: 0,
        disabled: true,
        component: {
          placeholder: '请选择版本状态'
        },
        rules: [ // 表单校验规则
          { required: true, message: '版本状态不能为空' }
        ],
        itemProps: {
          class: { yxtInput: true }
        }
      },

    },
      {
        fixed: 'right',
        title: '发布时间',
        key: 'release_time',
        width: 160,
        type: 'datetime',
        form: {
          value: new Date(),
          disabled: false,
          rules: [ // 表单校验规则
            { required: true, message: '发布时间不能为空' }
          ],
          itemProps: {
            class: { yxtInput: true }
          }
        }
      },
    ].concat(vm.commonEndColumns({    description: {
        showForm: false,
      },}))
  }
}
